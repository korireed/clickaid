NAME
Cory J. Reid

CLASS
UNM CS251.002

PROGRAM
Program 4: ClickAid

KNOWN BUGS
- [C] Not sure I like how I am capturing clicks and actioning the commands in
  actionPerformed(). Worried issues will arise when trying to add additional
  functionality or won't be verbose (pretty). Planning to assess this method
  in greater detail for "Enhanced" applet.
- [X] Board will reset each time "Start Over" is clicked rather than checking
  if board needs to be reset first.

TAGS KEY
[C] = Comment to grader
[X] = Minor bug (little impact / hardly noticeable)
[XX] = Severe bug (severe impact / noticeable)
[XXX] = Deadly bug (program killer / won't run / affects output)
