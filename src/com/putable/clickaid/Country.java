package com.putable.clickaid;

import javax.swing.*;

public class Country {

    private int miseryIndex;
    private int x, y;
    private JButton button;

    Country(int miseryIndex, int x, int y) {
        this.x = x;
        this.y = y;
        this.miseryIndex = miseryIndex;
        button = new JButton(new Integer(miseryIndex).toString());

    }

    public int getMiseryIndex() {
        return miseryIndex;
    }

    public JButton getButton() {
        return button;
    }

    public int[] getPosition() {
        return new int[]{x, y};
    }

    public void setMiseryIndex(int miseryIndex) {
        this.miseryIndex = miseryIndex;
        button.setText(new Integer(miseryIndex).toString());
    }

    public void decrementMiseryIndex(int numNonZero) {
        if (numNonZero > 0 && miseryIndex == 0) {
            miseryIndex = numNonZero;
        } else {
            miseryIndex--;
        }
        button.setText(new Integer(miseryIndex).toString());
    }

}
