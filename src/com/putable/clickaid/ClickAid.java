package com.putable.clickaid;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

public class ClickAid extends JApplet implements ActionListener {

    private int rows = 6; // default 6, minimum 3
    private int cols = 4; // default 4, minimum 3
    private Country[][] countries;
    private HashMap<JButton, Country> countryMap = new HashMap<>();
    private JButton resetButton;

    @Override
    public void init() {
        initGame();

        JPanel headerPanel = new JPanel(new GridLayout(2, 1));
        JPanel countryPanel = new JPanel(new GridLayout(rows, cols, 5, 5));

        setLayout(new BorderLayout());

        createCountries(countryPanel);
        headerPanel.add(resetButton, BorderLayout.NORTH);

        add(headerPanel, BorderLayout.NORTH);
        add(countryPanel, BorderLayout.CENTER);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == resetButton) {
            resetBoard();
        } else {
            sendAid(e);
        }
    }

    private void initGame() {
        int x = getParam("ROWS");
        int y = getParam("COLUMNS");

        if(x != -1) { rows = x; }
        if(y != -1) { cols = y; }

        resetButton = new JButton("Start Over");
        addListener(resetButton);
        countries = new Country[cols][rows];
    }

    private int getParam(String p) {
        int num;

        try {
            num = Integer.parseInt(this.getParameter(p));
        } catch (NumberFormatException e) {
            return -1;
        }

        if(num < 3) { num = 3; }

        return num;
    }

    private void addListener(JButton b) {
        b.addActionListener(this);
    }

    private void sendAid(ActionEvent e) {
        Country[] region = getRegion((JButton) e.getSource());
        int numNonZero = getNumNonZero(region);

        for (Country c : region) {
            if (numNonZero == 0) break;
            c.decrementMiseryIndex(numNonZero);
        }
    }

    private void resetBoard() {
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                int miseryIndex = (r + c) % 10;
                countries[c][r].setMiseryIndex(miseryIndex);
            }
        }
    }

    private Country[] getRegion(JButton button) {
        Country[] region = new Country[5];
        Country country = countryMap.get(button);
        int[][] locations = getPositions(country.getPosition());

        for (int i = 0; i < region.length; i++) {
            region[i] = countries[locations[i][0]][locations[i][1]];
        }

        return region;
    }

    private int getNumNonZero(Country[] region) {
        int numNonZero = 0;

        for (Country c : region) {
            if (c.getMiseryIndex() > 0) numNonZero++;
        }

        return numNonZero;
    }

    private int[][] getPositions(int[] start) {
        int[][] offset = {{0, -1}, {1, 0}, {0, 1}, {-1, 0}};
        int[][] positions = new int[5][2];

        positions[4] = start;

        for (int i = 0; i < offset.length; i++) {
            int x = (start[0] + offset[i][0]) % cols;
            int y = (start[1] + offset[i][1]) % rows;

            if (x < 0) x = cols - 1;
            if (y < 0) y = rows - 1;

            int[] position = {x, y};

            positions[i] = position;
        }

        return positions;
    }

    private void createCountries(JPanel countryPanel) {
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                int miseryIndex = (r + c) % 10;
                Country country = new Country(miseryIndex, c, r);

                countries[c][r] = country;
                countryMap.put(country.getButton(), country);
                addListener(country.getButton());
                countryPanel.add(country.getButton());
            }
        }
    }

}
